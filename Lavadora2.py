from duplicity.log import _ElapsedSecs2Str

__author__ = 'slenderman'

import random


class Lavadora:
    def __init__(self):
        self.cromosoma = [0, 0, 0, 0, 0, 0]
        self.prioridad = 0

    def __init__(self, ADN, prioridad):
        self.cromosoma = ADN
        self.prioridad = prioridad

    def __repr__(self):
        return "Lavadora(ADN={}, Prioridad={})".format(self.cromosoma, self.prioridad)

    def __str__(self):
        return ("Lavadora{ADN={}, Prioridad={})".format(self.cromosoma, self.prioridad))

    def getCromosoma(self):
        return self.cromosoma

    def getPrioridad(self):
        return self.prioridad

    def setPrioridad(self, valor):
        self.prioridad = valor


    def cruzamiento(self, lavadora2, dado):
        print dado
        listLavadora = []
        adn1 = [0,0,0,0,0,0]
        adn2 = [0,0,0,0,0,0]

        for i in range(0, len(lavadora2.getCromosoma())):
            if (i < dado):
                adn1[i] = lavadora2.getCromosoma()[i]
                adn2[i] = self.getCromosoma()[i]

        for i in range(0, len(lavadora2.getCromosoma())):
            if (i >= dado):
                adn1[i] = self.getCromosoma()[i]

            if (i >= dado):
                adn2[i] = lavadora2.getCromosoma()[i]

        listLavadora = [Lavadora(adn2, lavadora2.getPrioridad()),Lavadora(adn1, self.getPrioridad())]

        return listLavadora

    def mutacionGenes(self):
        selecionarGen = random.randint(0, 6)
        probabilidadMutacion = random.randint(1, 100)

        if (probabilidadMutacion >= 0 and probabilidadMutacion <= 50):
            self.getCromosoma()[selecionarGen] = random.randint(0,100)
            cromosoma = self.getCromosoma()
        else:
            cromosoma =self.getCromosoma()
        return cromosoma

    def funcionMalaLavadora(self):
        pesoCarga = self.cromosoma[0]
        pesoPermitido = self.cromosoma[1]
        nivelDeterg = self.cromosoma[2]
        nivelAgua = self.cromosoma[3]
        tempAgua = self.cromosoma[4]
        progLavado = self.cromosoma[5]
        #if (pesoPermitido != 0):
        proporcion = (pesoCarga / pesoPermitido)
        valLavado = (nivelDeterg + nivelAgua + tempAgua) * progLavado
        funcion = i( proporcion * valLavado)

        return funcion





