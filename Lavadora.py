__author__ = 'slenderman'

import random


class Lavadora:
    def __init__(self, ADN):
        self.progLavado = ADN[0]
        self.pesoCarga = ADN[1]
        self.pesoPermitido = ADN[2]
        self.nivelAgua = ADN[3]
        self.nivelDeterg = ADN[4]
        self.tempAgua = ADN[5]
        seleccion = False


    def __repr__(self):
        return "AD{}-{}-{}-{}-{}-{}".format(self.progLavado, self.pesoCarga, self.pesoPermitido, self.nivelAgua,
                                          self.nivelDeterg, self.tempAgua)

    def __str__(self):
        return ("{}-{}-{}-{}-{}-{}".format(self.progLavado, self.pesoCarga, self.pesoPermitido, self.nivelAgua,
                                           self.nivelDeterg, self.tempAgua))

    def getSeleccion(self):
        return self.seleccion

    def setSeleccion(self, cruza):
        self.seleccion = cruza

    def getCromosoma(self):
        gen = None
        return gen[self.progLavado, self.pesoCarga, self.pesoPermitido, self.nivelAgua, self.nivelDeterg, self.tempAgua]

    def getCabeza(self):
        cabeza = None
        return cabeza[self.progLavado, self.pesoCarga, self.pesoPermitido]

    def getCuerpo(self):
        return (self.nivelAgua, self.nivelDeterg, self.tempAgua)

    def mutacionGenes(cls, sujeto):
        selecionarGen = random.randint(0, 6)
        probabilidadMutacion = random.randint(1, 100)

        if (probabilidadMutacion >= 0 and probabilidadMutacion <= 6):
            sujeto[selecionarGen] = random.randint(0, 1)

            return sujeto

    def elMejor(cls, gananciaPorCromosoma):
        temperaturaMayor = 0
        indice = 0
        # ver para cambiar
        for i in range(0, 6):
            temperatura = gananciaPorCromosoma[i]
            if temperatura > temperaturaMayor:
                temperaturaMayor = temperatura
                indice = i
        return indice

    def combinar(cls, invertir, sujeto1, sujeto2):
        if (not invertir):
            hijo = list(sujeto1.getCabeza())
            hijo.extend(list(sujeto2.getCuerpo()))
        else:
            hijo = list(sujeto2.getCabeza())
            hijo.extend(list(sujeto1.getCuerpo()))
        return hijo


def funcionMalaLavadora(lavadora):
    proporcion = (lavadora.pesoCarga / lavadora.pesoPermitido)
    valLavado = (lavadora.nivelDeterg + lavadora.nivelAgua + lavadora.tempAgua) * lavadora.progLavado
    return proporcion * valLavado


def cruzamiento(self, lavadora2, dado):
    adn1 = None
    adn2 = None

    for i in len(lavadora2.getCromosoma()):
        if (i < dado):
            adn1[i] = lavadora2.getCromosoma()[i]
            adn2[i] = self.getCromosoma()[i]
        get


